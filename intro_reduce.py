
from functools import reduce


numeros = [10,10,10,10]

sumar = lambda acumulador,elemento: acumulador+elemento

resp = reduce(sumar, numeros)
print(resp)

mensaje = "Hola mundo"
lista_mensaje = list(mensaje)
print(lista_mensaje)

resp = reduce( lambda acumulador, caracter: acumulador+caracter,  lista_mensaje)
print(resp)

print('----------------')

numeros = [10,10,10,10]
resp = reduce( lambda acumulador, elemento: str(acumulador)+' '+str(elemento), numeros  )
print(resp)


