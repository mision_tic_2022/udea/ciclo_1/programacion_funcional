

def fabrica_operaciones(operador: str):
    funcion = lambda n1,n2: f'No existe operación para {n1} {operador} {n2}'
    if operador == '+':
        funcion = lambda n1,n2: n1+n2
    elif operador == '-':
        funcion = lambda n1,n2: n1-n2
    elif operador == '*':
        funcion = lambda n1,n2: n1*n2
    elif operador == '/':
        funcion = lambda n1,n2: n1/n2
    elif operador == '**':
        funcion = lambda n1,n2: n1**n2
    
    return funcion

def mapear_datos(funcion, lista: list):
    resultados = []
    for t in lista:
        n1,n2 = t
        x = funcion(n1,n2)
        resultados.append(x)

    return resultados

lista_tuplas = [(10,20), (20,30), (40,50), (60,70)]

potencia = fabrica_operaciones('+')
resp = mapear_datos(potencia, lista_tuplas)
print(resp)



""" try:
    operacion = fabrica_operaciones('%')
    print( operacion(10,10) )
except:
    print('Operación inválida') """
