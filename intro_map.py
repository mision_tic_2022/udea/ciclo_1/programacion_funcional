
elevar_cuadrado = lambda n: n**2

def mitad(n):
    return n/2

numeros = [10,20,30,40,50,60,70,80,90]

respuesta = list(map(mitad, numeros))

print(respuesta)

respuesta = list(map(lambda n: n**2, numeros))
print(respuesta)

'''
    1) Mapear la lista 'nombres' para que retorne una lista con los nombres
        en mayúscula
    2) Mapear la lista 'nombres' para que retorne una lista con las iniciales
        de cada nombre
    3) Mapear la lista 'nombres' para que retorne una lista de tuplas, cada
        tupla representa a cada nombre de la lista ejemplo:
        ['Juan', '']
        retorna: [ ('J','u','a','n'), (...), ... ]
    NOTA:
        *Para todos los casos usar funciones anónimas (lambda)
        *La solución no debe llevar mas de 1 linea de código
'''
nombres = ['juan', 'maría', 'juliana', 'danilo']

#Danilo
respuesta1 = list(map(lambda n: n.upper(),nombres))
print(respuesta1)

#Santiago
nuevos_nombres = list(map(lambda nombre: nombre.upper(), nombres))

#Jesus
nombres_upper = list(map(lambda n:n.upper(), nombres))
print(nombres_upper)

#Gilberto
nombresMayuscula = [ n.upper() for n in nombres]

respueta_1 = list(map(lambda n: n.upper(), nombres))


#------------Punto #2
p2 = list(map(lambda nombre: nombre[0].upper(), nombres))

iniciales = list(map(lambda inicial: inicial[0], nombres))
respuesta_2 = list(map(lambda n: n[0], nombres))

#---------------------Punto #3
respuesta3 = list(map(lambda n: tuple(map(lambda i: i[0], n)), nombres))
print(respuesta3)
punto3 = list( map( lambda n: tuple(n) , nombres) )