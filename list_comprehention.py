
numeros = [10,20,30,40,50,60,70,80,90]

resultado = [n**2 for n in numeros ]

print(resultado)

nombres = ['Juan', 'María', 'Juliana', 'Danilo']
iniciales = [n[0].lower() for n in nombres ]
print(iniciales)

numeros = [10,15,20,25,30,35,40,45,50,60,70,80,90]

pares = [n for n in numeros if n%2==0]
print(pares)

impares_en_cero = [n if n%2==0 else 0 for n in numeros  ]
print(impares_en_cero)

